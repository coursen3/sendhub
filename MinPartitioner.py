class MinPartitioner:
    '''
    Partitions a value into the minimum number of
    partitions possible such that each partition
    has a size drawn from a specifiable list of
    acceptable partition sizes

    Inputs:
    PARTS - List of acceptable partition sizes
    N - Maximum expected value to partition
    '''
    def __init__(self, parts, N=5000):
        self.parts = parts
        self.minp = []
        self._build_minp(N)

    '''
    Build table of all possible partitions from 0 to N
    Reusing results from previous call

    Inputs:
    N - Maximum value to build the table to
    '''
    def _build_minp(self, N):
        if len(self.minp) < 1:
            self.minp.append(());

        for val in range(len(self.minp), N+1):
            best = None
            for p in [i for i in self.parts if i <= val]:
                rem = self.minp[val-p]
                if rem is None: continue

                if best is None or (len(rem) + 1 < len(best)):
                    best = rem + (p,)

            self.minp.append(best)

    '''
    Partition N and return a tuple of the partition sizes
    Inputs:
        N - The value to partition

    Output:
        Tuple of partition values
    '''
    def partition(self, N):
        if N >= len(self.minp):
            return None;

        return self.minp[N];

if __name__ == '__main__':
        part = MinPartitioner([1, 5, 10, 25])