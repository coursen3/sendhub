#Notes#
Unfortunately I did not have time to get this up and running on AWS.  This was due to a combination of personal time constraints and the fact that it was my first experience with REST and Flask (and web frameworks in general) so there was a bit of a learning curve on the "systems" portion of the task.  I feel that success on this portion of the task is determined more by knowledge / experience than programming ability and I am confident that I could get up to speed quickly enough that it would not be an issue should you decide to offer me a position. That said, I fully understand that this falls short of the requirements.

The REST API here has only a single end-point which accepts a POST request.  I realize the stated task aligns more to a GET semantically, but I decided to use POST due to the size of the request data.  The request could include up to 5000 phone numbers.  Some cursory research on the web indicated that data in a GET message should be in the URL (not message body) and that it would be problematic to allow URLs of lengths greater than 2000 characters.  Therefore a POST request was used to ensure the data can be safely transferred.  It would be possible (and potentially more RESTful) to use a POST request only to send the request, and then follow up with a GET request to obtain the response.  However, this requires the server to cache the response and that seems like an unncessary complication.

I also found that Google's translate API handled a similar scenario by using a POST request: http://stackoverflow.com/questions/19771031/rest-request-cannot-be-encoded-for-get-url-too-long

I utilized a basic phone number validation regular expression, modified from one found on a stack overflow post.  I also compared this to Googles libphonenumber.  Both of them considered "+15555555555" to be a valid phone number, which contradicts a note in the instructions.  I did not dig deeper into this so my current implementation will pass 555 numbers.

#Dependencies#
This implementation utilizes the Flask web framework

#Testing#
To run the endpoint, start the app on localhost:
python app.py

To run some test cases (requires app running on local host):
python test.py -v

To see other edge cases that can be tested:
python test.py -h

#Complexity#
i. / ii. The routing algorithm is frontloaded so that the optimal solution for any number of recipients, N up to 5000, is cached on startup.   This makes the time complexity of *determining* the routing O(1) and the space complexity O(N).  Technically, writing the JSON message after the routing is determined is O(N) where N is the number of recipients.  

Practically speaking, the algorithm provided could be easily extended to handle requests over 5000 with a potentially suboptimal answer by simply applying a greedy approach (using the highest throughput relay) until the remaining number of requests is below 5000.

iii. This is a classic dynamic programming example.  It's similar to making exact change with the least number of coins possible.

iv.  This algorithm runs in better than polynomial time due to caching.  If caching was not allowed to persist between calls, the algorihtm would run in polynomial time and O(N) space for each invocation.  This algorithm is optimal for any combination of relays / throughputs.  A simpler, greedy approach, would be possible for this particular set of relays.