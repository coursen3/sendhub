import re
import string


'''
Validate input JSON, returns (True, "") if valid,
(False, <error message>) otherwise
'''
requiredJSON = ['message', 'recipients'];
def validate(request):
    #alidVate required fields are present
    for f in requiredJSON:
        if f not in request:
            return (False, "Required field missing: '{0}'".format(f))

    #Validate phone numbers
    clean_rec = []
    for p in request['recipients']:
        valid, pclean = valid_phone_number(p)
        if not valid:
            return (False, "Invalid phone number: '{0}'".format(p))
        clean_rec.append(pclean)

    request['recipients'] = clean_rec
    return (True, "")


'''
Validate US phone number
'''
countrycode = "(?:\\+1)"
area = "(?:([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9]))"
digits = "([2-9]1[02-9]|[2-9][02-9]1|[2-9][02-9]{2})([0-9]{4})"
reg = re.compile(countrycode + area + digits)
remove = "-.()"
trans = dict((ord(c),None) for c in remove)

def valid_phone_number(s):
    s = "".join(s.split())
    s = s.translate(trans)
    if reg.match(s):
        return (True, s)
    return (False, s)