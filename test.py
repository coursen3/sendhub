import requests, json, argparse
parser = argparse.ArgumentParser(description='Test app.py (requires app.py to be running on localhost)')
parser.add_argument('-v', action='store_true', help='Verbose output')
parser.add_argument('--badnumber', action='store_true', help='Use bad phone numbers')
parser.add_argument('--norecipients', action='store_true', help='Send request w/o recipients')
parser.add_argument('--nomessage', action='store_true', help='Send request w/o message')
parser.add_argument('--toomany', action='store_true', help='Send request w/ too many recipients')
args = parser.parse_args()

url = "http://127.0.0.1:5000/router"
headers = {'Content-type': 'application/json', 'Accept': 'application/json'}
msg = "Test"

 #Number of recipients and expected requests required
if args.toomany:
    rec = [5001]
    req = [0]
else:
    rec = [20, 25, 26, 30] 
    req = [2, 1, 2, 2]

err = args.badnumber or args.nomessage or args.norecipients or args.toomany

for reci, reqi in zip(rec,req):
    rcp = []
    #Build recipient list
    start = 15555555555
    if args.badnumber:
        start = start/10

    for i in range(start,start+reci):
        rcp.append(format(i,'+011'))

    #Build and POST request
    data = dict()
    if not args.nomessage:
        data["message"] = msg
    if not args.norecipients:
        data["recipients"] = rcp
    data = json.dumps(data)

    res = requests.post(url, data, headers=headers)

    if args.v:
        print "Request JSON:", data

    if not res.ok:
        print "Response FAIL:", res.text
        continue

    reqi_json = res.json()
    if args.v:
        print "Response JSON:", res.json()
    
    #Compare repsonse to expected
    if "errors" in reqi_json and err:
        print "PASS with error: '{1}'".format(reci, reqi_json["errors"])

    else:
        reqi_res = len(reqi_json['routes'])
        if reqi_res != reqi:
            print "FAIL with {0} recipients.  Expected {1}, got {2}".format(reci, reqi, reqi_res)
        else:
            print "PASS with {0} recipients, {1} requests".format(reci, reqi)