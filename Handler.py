import json
from MinPartitioner import MinPartitioner
from validation import validate

class Handler:
    '''
    Handles an incoming JSON request
    '''
    rMax = 5000

    def __init__(self, relays):
        #Build map throughput -> list of relays
        #Build map throughput -> last relay used
        self.rmap = dict()
        self.rlast = dict()
        for r in relays:
            if r.tp not in self.rmap: 
                self.rmap[r.tp] = []
                self.rlast[r.tp] = 0

            self.rmap[r.tp].append(r)

        self.partitioner = MinPartitioner(self.rmap.keys(), self.rMax)

    '''
    Handle JSON request, returning success message or error message
    '''
    def handle(self, request):
        valid = validate(request)
        if not valid[0]:
            return self.error(valid[1])

        #Get recipients and message
        rcp = request['recipients']
        msg = request['message']
        
        #Partition recipients into requests
        i = 0
        routes = []

        parts = self.partitioner.partition(len(rcp))
        if parts is None:
            return self.error("Number of recipients must be 0-{0}".format(self.rMax))

        for p in parts:
            routes.append(self.build_route(rcp[i:i+p]))
            i += p

        #Return successful response
        return self.success(msg, routes)

    '''
    Build error message
    '''
    def error(self, msg):
        #TODO
        return json.dumps({"errors":[{"message": msg}]})
    
    '''
    Build JSON routing request
    '''
    def build_route(self, rcp):
        #Select relay
        p = len(rcp)
        ri = (self.rlast[p] + 1) % len(self.rmap[p])
        relay = self.rmap[p][ri];

        #Update rlast
        self.rlast[p] = ri

        return {"ip": relay.ip, "recipients": rcp}


    '''
    Build success message
    '''
    def success(self, msg, routes):
        return json.dumps({"message": msg, "routes": routes})


class Relay:
    '''
    Helper class to hold basic relay info
    '''
    def __init__(self, tp, ip):
        self.tp = tp
        self.ip = ip


if __name__ == '__main__':
    relays = []
    relays.append(Relay(1, '10.0.1.0'))
    relays.append(Relay(5, '10.0.2.0'))
    relays.append(Relay(10, '10.0.3.0'))
    relays.append(Relay(25, '10.0.4.0'))
    h = Handler(relays)

    msg = u'test'
    rcp = []
    N = 7
    start = 15555555555
    for i in range(start,start+N):
        rcp.append(unicode(format(i,'+011')))

    req = {'message': msg, 'recipients':rcp}
    
    print h.handle(req)