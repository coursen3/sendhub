from flask import Flask, request, json
from Handler import Handler, Relay
app = Flask(__name__)

relays = [Relay(1, '10.0.1.0'), 
    Relay(5, '10.0.2.0'), 
    Relay(10, '10.0.3.0'),
    Relay(25, '10.0.4.0')]

h = Handler(relays)

#Create /router POST resource
@app.route('/router', methods = ['POST'])
def route():
    if request.headers['Content-Type'] == 'application/json' \
        and request.headers['Accept'] == 'application/json':
        return h.handle(request.json)
    else:
        return "415 Unsupported Media Type"

if __name__ == '__main__':
    app.run()